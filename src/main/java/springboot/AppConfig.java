package springboot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springboot.repositories.CinemaRepository;
import springboot.repositories.FilmRepository;
import springboot.repositories.SalleRepository;
import springboot.repositories.SeanceRepository;
import springboot.services.CinemaService;
import springboot.services.CinemaServiceImpl;
import springboot.services.FilmService;
import springboot.services.FilmServiceImpl;
import springboot.services.SalleService;
import springboot.services.SalleServiceImpl;
import springboot.services.SeanceService;
import springboot.services.SeanceServiceImpl;

@Configuration
public class AppConfig {
	@Bean
	public CinemaService cinemaService(CinemaRepository cinemaRepository) {
		return new CinemaServiceImpl(cinemaRepository);
	}
	
	@Bean
	public FilmService filmService(FilmRepository filmRepository) {
		return new FilmServiceImpl(filmRepository);
	}
	
	@Bean
	public SalleService salleService(SalleRepository salleRepository) {
		return new SalleServiceImpl(salleRepository);
	}
	
	@Bean
	public SeanceService seanceService(SeanceRepository seanceRepository) {
		return new SeanceServiceImpl(seanceRepository);
	}
}
