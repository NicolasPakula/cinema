package springboot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import springboot.models.Film;

public interface FilmRepository extends MongoRepository<Film, String>{

}
