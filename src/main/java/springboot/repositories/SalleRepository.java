package springboot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import springboot.models.Salle;

public interface SalleRepository extends MongoRepository<Salle, String>{

}
