package springboot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import springboot.models.Cinema;

public interface CinemaRepository extends MongoRepository<Cinema, String>{

}
