package springboot.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import springboot.models.Seance;

public interface SeanceRepository extends MongoRepository<Seance, String>{

}
