package springboot.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Document
public class Salle {
	@Id
	private String id;
	private Integer numero;
	private Integer nbrPlaces;
	@DBRef
	@Field("cinema")
	private Cinema cinema;
}
