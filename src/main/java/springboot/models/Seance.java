package springboot.models;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Document
public class Seance {
	@Id
	private String id;
	private LocalDateTime date;
	@DBRef
	@Field("salle")
	private Salle salle;
	@DBRef
	@Field("film")
	private Film film;
	
}
