package springboot.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Salle;
import springboot.services.SalleService;

@RestController
@CrossOrigin
@RequestMapping("/salles")
public class SalleController {
	private SalleService salleService;
	
	public SalleController(SalleService salleService) {
		this.salleService = salleService;
	}
	
	@GetMapping
	public List<Salle> getAll(){
		return this.salleService.getAll();
	}
	
	@GetMapping("{id}")
	public Salle getById(@PathVariable String id) {
		return this.salleService.getById(id);
	}
	
	@PostMapping
	public Salle create(@RequestBody Salle salle) {
		return this.salleService.create(salle);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable String id) {
		this.salleService.delete(id);
	}
	
	@PutMapping
	public Salle put(@RequestBody Salle salle) {
		return this.salleService.put(salle);
	}
	
	@PatchMapping("{id}")
	public Salle patch(@RequestBody Salle salle,@PathVariable String id) {
		return this.salleService.patch(salle,id);
	}
}
