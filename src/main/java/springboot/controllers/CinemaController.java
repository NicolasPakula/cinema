package springboot.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Cinema;
import springboot.services.CinemaService;

@RestController
@CrossOrigin
@RequestMapping("/cinema")
public class CinemaController {
	private CinemaService cinemaService;
	
	public CinemaController (CinemaService cinemaService) {
		this.cinemaService = cinemaService;
	}
	
	@GetMapping
	public List<Cinema> getAll(){
		return cinemaService.getAll();
	}
	
	@GetMapping("{id}")
	public Cinema getById(@PathVariable String id) {
		return cinemaService.getCinemaById(id);
	}
	
	@PostMapping
	public Cinema create(@RequestBody Cinema cinema) {
		return cinemaService.create(cinema);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable String id) {
		cinemaService.delete(id);
	}
	
	@PutMapping
	public Cinema put(@RequestBody Cinema cinema) {
		return cinemaService.put(cinema);
	}
	
	@PatchMapping("{id}")
	public Cinema patch(@RequestBody Cinema cinema,@PathVariable String id) {
		return cinemaService.patch(cinema,id);
	}
}
