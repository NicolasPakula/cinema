package springboot.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Seance;
import springboot.services.SeanceService;

@RestController
@CrossOrigin
@RequestMapping("/seances")
public class SeanceController {
	private SeanceService seanceService;
	
	public SeanceController(SeanceService seanceService) {
		this.seanceService = seanceService;
	}
	
	@GetMapping
	public List<Seance> getAll(){
		return seanceService.getAll();
	}
	
	@GetMapping("{id}")
	public Seance getById(@PathVariable String id) {
		return seanceService.getById(id);
	}
	
	@PostMapping
	public Seance create(@RequestBody Seance seance) {
		return seanceService.create(seance);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable String id) {
		this.seanceService.delete(id);
	}
	
	@PutMapping
	public Seance put(@RequestBody Seance seance) {
		return this.seanceService.put(seance);
	}
	
	@PatchMapping("{id}")
	public Seance patch(@RequestBody Seance seance, @PathVariable String id) {
		return this.seanceService.patch(seance,id);
	}
}
