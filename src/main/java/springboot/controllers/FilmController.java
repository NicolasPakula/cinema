package springboot.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import springboot.models.Film;
import springboot.services.FilmService;

@RestController
@CrossOrigin
@RequestMapping("/films")
public class FilmController {
	private FilmService filmService;
	
	public FilmController(FilmService filmService) {
		this.filmService = filmService;
	}
	
	@GetMapping
	public List<Film> getAll(){
		return this.filmService.getAll();
	}
	
	@GetMapping("{id}")
	public Film getById(@PathVariable String id) {
		return this.filmService.getById(id);
	}
	
	@PostMapping
	public Film create(@RequestBody Film film) {
		return this.filmService.create(film);
	}
	
	@DeleteMapping("{id}")
	public void delete(@PathVariable String id) {
		this.filmService.delete(id);
	}
	
	@PutMapping
	public Film put(@RequestBody Film film) {
		return this.filmService.put(film);
	}
	
	@PatchMapping("{id}")
	public Film patch(@RequestBody Film film, @PathVariable String id) {
		return this.filmService.patch(film,id);
	}
}
