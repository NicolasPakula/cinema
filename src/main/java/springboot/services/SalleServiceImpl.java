package springboot.services;

import java.util.List;

import springboot.models.Salle;
import springboot.repositories.SalleRepository;

public class SalleServiceImpl implements SalleService{

	private SalleRepository salleRepository;
	
	public SalleServiceImpl(SalleRepository salleRepository) {
		this.salleRepository = salleRepository;
	}
	
	@Override
	public List<Salle> getAll() {
		return this.salleRepository.findAll();
	}

	@Override
	public Salle getById(String id) {
		return this.salleRepository.findById(id).orElse(null);
	}

	@Override
	public Salle create(Salle salle) {
		return this.salleRepository.save(salle);
	}

	@Override
	public void delete(String id) {
		this.salleRepository.deleteById(id);
	}

	@Override
	public Salle put(Salle salle) {
		return this.salleRepository.save(salle);
		
	}

	@Override
	public Salle patch(Salle salle, String id) {
		Salle salleTemp = this.salleRepository.findById(id).orElse(null);
		if(salle.getNumero()!=null) salleTemp.setNumero(salle.getNumero());
		if(salle.getNbrPlaces()!=null) salleTemp.setNbrPlaces(salle.getNbrPlaces());
		if(salle.getCinema().getId()!=null) salleTemp.setCinema(salle.getCinema());
		return this.salleRepository.save(salleTemp);
		
	}
	
	

}
