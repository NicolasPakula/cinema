package springboot.services;

import java.util.List;

import springboot.models.Film;

public interface FilmService {
	public List<Film> getAll();
	public Film getById(String id);
	public Film create(Film film);
	public void delete(String id);
	public Film put(Film film);
	public Film patch(Film film, String id);
}
