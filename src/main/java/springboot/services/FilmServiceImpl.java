package springboot.services;

import java.util.List;

import springboot.models.Film;
import springboot.repositories.FilmRepository;

public class FilmServiceImpl implements FilmService{

	private FilmRepository filmRepository;
	
	public FilmServiceImpl(FilmRepository filmRepository) {
		this.filmRepository = filmRepository;
	}
	
	@Override
	public List<Film> getAll() {
		return filmRepository.findAll();
	}

	@Override
	public Film getById(String id) {
		return filmRepository.findById(id).orElse(null);
	}

	@Override
	public Film create(Film film) {
		return filmRepository.save(film);
	}

	@Override
	public void delete(String id) {
		filmRepository.deleteById(id);
	}

	@Override
	public Film put(Film film) {
		return filmRepository.save(film);
	}

	@Override
	public Film patch(Film film, String id) {
		Film filmTemp = this.filmRepository.findById(id).orElse(null);
		if(film.getDuree()!=null) filmTemp.setDuree(film.getDuree());
		if(film.getNom()!=null) filmTemp.setNom(film.getNom());
		return this.filmRepository.save(filmTemp);
	}

}
