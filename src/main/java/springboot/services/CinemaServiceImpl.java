package springboot.services;

import java.util.List;

import springboot.models.Cinema;
import springboot.repositories.CinemaRepository;

public class CinemaServiceImpl implements CinemaService{
	private CinemaRepository cinemaRepository;
	
	public CinemaServiceImpl(CinemaRepository cinemaRepository) {
		this.cinemaRepository = cinemaRepository;
	}

	@Override
	public List<Cinema> getAll() {
		return cinemaRepository.findAll();
	}

	@Override
	public Cinema getCinemaById(String id) {
		return cinemaRepository.findById(id).orElse(null);
	}

	@Override
	public Cinema create(Cinema cinema) {
		return cinemaRepository.save(cinema);
	}

	@Override
	public void delete(String id) {
		cinemaRepository.deleteById(id);
		
	}

	@Override
	public Cinema put(Cinema cinema) {
		return cinemaRepository.save(cinema);
	}

	@Override
	public Cinema patch(Cinema cinema, String id) {
		Cinema cinemaTemp = this.cinemaRepository.findById(id).orElse(null);
		if(cinema.getNom()!=null) cinemaTemp.setNom(cinema.getNom());
		return this.cinemaRepository.save(cinemaTemp);
	}
	
	
}
