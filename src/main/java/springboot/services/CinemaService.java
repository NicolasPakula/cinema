package springboot.services;

import java.util.List;

import springboot.models.Cinema;

public interface CinemaService {
	public List<Cinema> getAll();
	public Cinema getCinemaById(String id);
	public Cinema create(Cinema cinema);
	public void delete(String id);
	public Cinema put(Cinema cinema);
	public Cinema patch(Cinema cinema, String id);
}
