package springboot.services;

import java.util.List;

import springboot.models.Seance;
import springboot.repositories.SeanceRepository;

public class SeanceServiceImpl implements SeanceService{

	private SeanceRepository seanceRepository;
	
	public SeanceServiceImpl(SeanceRepository seanceRepository) {
		this.seanceRepository = seanceRepository;
	}
	
	@Override
	public List<Seance> getAll() {
		return seanceRepository.findAll();
	}

	@Override
	public Seance getById(String id) {
		return seanceRepository.findById(id).orElse(null);
	}

	@Override
	public Seance create(Seance seance) {
		return seanceRepository.save(seance);
	}

	@Override
	public void delete(String id) {
		seanceRepository.deleteById(id);
		
	}

	@Override
	public Seance put(Seance seance) {
		return this.seanceRepository.save(seance);
	}

	@Override
	public Seance patch(Seance seance,String id) {
		Seance seanceTemp = this.seanceRepository.findById(id).orElse(null);
		if(seance.getDate()!=null)seanceTemp.setDate(seance.getDate());
		if(seance.getFilm().getId()!=null)seanceTemp.setFilm(seance.getFilm());
		if(seance.getSalle().getId()!=null)seanceTemp.setSalle(seance.getSalle());
		return seanceRepository.save(seanceTemp);
	}
	

}
