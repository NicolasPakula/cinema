package springboot.services;

import java.util.List;

import springboot.models.Salle;

public interface SalleService{
	public List<Salle> getAll();
	public Salle getById(String id);
	public Salle create(Salle Salle);
	public void delete(String id);
	public Salle put(Salle salle);
	public Salle patch(Salle salle, String id);
}
