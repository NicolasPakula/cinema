package springboot.services;

import java.util.List;

import springboot.models.Seance;

public interface SeanceService {
	public List<Seance> getAll();
	public Seance getById(String id);
	public Seance create(Seance seance);
	public void delete(String id);
	public Seance put(Seance seance);
	public Seance patch(Seance seance, String id);
}
